import math
import sys
file_input=str(sys.argv[1])
#file_output=str(sys.argv[2])
file_testing=str(sys.argv[2])
#open the model file
model = open(file_input,'r',errors='ignore')
#open the output file 
#output = open(file_output,'w+',errors='ignore')
#open the input file
input = open(file_testing,'r',errors='ignore')
#create a dictionary to put the probability for each class
#prob_dict={}
class_probabilities={}
class_identity={}
class_words_probabilities={}
#read each line in the input file and store in a list 'input_words'
input_words=[]
class_index=0
for line in model:
 if line.startswith("_CLASS_"):
  words=line.split()
  #print (words)
  class_identity[class_index]=words[1]
  class_probabilities[class_index]=words[2]
  class_words_probabilities[class_index]={}
  class_index+=1
  line=next(model)
 l=line.split()
 class_words_probabilities[class_index-1][l[0]]=(l[1])
prob_res_class={}
for line in input:
 words=line.split()
 prob_res={}
 for i in class_identity:
  for w in words:
    if i not in prob_res:
     prob_res[i]=math.log(float(class_probabilities[i]))
    if w in class_words_probabilities[i]:
     prob_res[i]+=math.log(float(class_words_probabilities[i][w]))
    if w not in class_words_probabilities[i]:
     prob_res[i]+=math.log(float(class_words_probabilities[i]['Unknown_*****_Nivu_UNknown_Label'])) 
 result=max(prob_res,key=prob_res.get)
 #output.write(class_identity[result])
 #output.write("\n")  
 print(class_identity[result])
 #print('\n')

